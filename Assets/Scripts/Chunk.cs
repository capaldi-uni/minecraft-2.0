﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chunk  //piece of world of defined size, contains blocks
{
    public Coordinates coord;
    World world;

    bool _isActive;
    public bool isActive
    {
        get { return _isActive; }
        set 
        { 
            _isActive = value;
            if( !(chunkObject is null) )
                chunkObject.SetActive(value); 
        }
    }
    public Vector3 position
    {
        get 
        {
            if( !(chunkObject is null))
                return chunkObject.transform.position;
            else
                return new Vector3(coord.x * Minecraft.ChunkW, 0, coord.z * Minecraft.ChunkW); 
        }
    }

    GameObject chunkObject;
    MeshRenderer mRenderer;
    MeshFilter mFilter;
    
    List<Vector3> vertices = new List<Vector3>();   //vertices of mesh
    List<Vector2> uvs = new List<Vector2>();        //texture coordinates
    List<int> triangles = new List<int>();          //triangles of mesh
    int vIndex = 0;     //index of added vertices 

    //all the blocks of chunk
    short[,,] blockMap = new short[Minecraft.ChunkW, Minecraft.ChunkH, Minecraft.ChunkW];  
    public bool isBlockMap = false;

    public Queue<BlockMod> modifications = new Queue<BlockMod>();   //blocks to add after terrain generated, like in world

    public Chunk(World _world, Coordinates _coord, bool onCreateGeneration)
    {
        world = _world;
        coord = _coord;
        _isActive = true;

        if(onCreateGeneration)
            GenerateChunk();
    }

    public void GenerateChunk()
    {
        //creating game object to represent and render chunk
        chunkObject = new GameObject();
        mFilter = chunkObject.AddComponent<MeshFilter>();
        mRenderer = chunkObject.AddComponent<MeshRenderer>();

        mRenderer.material = world.textureMap;  //chunk gets world's texture
        chunkObject.transform.SetParent(world.transform);   //and its position is relative to world's one
        chunkObject.transform.position = new Vector3(coord.x * Minecraft.ChunkW, 0f, coord.z * Minecraft.ChunkW);
        chunkObject.name = "Chunk on [" + coord.x + ", " + coord.z + "]";   //name to keep track

        //generating chunk and creating mesh
        GenerateBlockMap();
        UpdateChunk();
    }

    void GenerateBlockMap()
    {
        for(int x = 0; x < Minecraft.ChunkW; x++)
        {
            for(int z = 0; z < Minecraft.ChunkW; z++)
            {
                for(int y = 0; y < Minecraft.ChunkH; y++)
                {
                    blockMap[x, y, z] = world.GetNewBlock(position + new Vector3(x, y, z));
                }
            }
        }

        isBlockMap = true;
    }
    
    bool IsInChunk(int x, int y, int z)
    {
        return !(x < 0 || x >= Minecraft.ChunkW || 
                 y < 0 || y >= Minecraft.ChunkH || 
                 z < 0 || z >= Minecraft.ChunkW);
    }
    bool IsInChunk(Vector3 pos)
    {
        int x = (int)pos.x;
        int y = (int)pos.y;
        int z = (int)pos.z;
        return !(x < 0 || x >= Minecraft.ChunkW || 
                 y < 0 || y >= Minecraft.ChunkH || 
                 z < 0 || z >= Minecraft.ChunkW);
    }
    
    bool IsSolid(Vector3 pos)
    {
        int x = Mathf.FloorToInt(pos.x);
        int y = Mathf.FloorToInt(pos.y);
        int z = Mathf.FloorToInt(pos.z);
        if(IsInChunk(x, y, z))
            return world.blockTypes[blockMap[x, y, z]].isSolid;
        else
        {
            short block = world.GetBlock(position + pos);
            if(block >= 0)
                return world.blockTypes[block].isSolid;
            else
                return false;
        }
    }
    bool IsOpaque(Vector3 pos)
    {
        int x = Mathf.FloorToInt(pos.x);
        int y = Mathf.FloorToInt(pos.y);
        int z = Mathf.FloorToInt(pos.z);
        if(IsInChunk(x, y, z))
            return world.blockTypes[blockMap[x, y, z]].isOpaque;
        else
        {
            short block = world.GetBlock(position + pos);
            if(block >= 0)
                return world.blockTypes[block].isOpaque;
            else
                return false;
        }
    }
    
    public short GetBlock(Vector3 globalPos)    //get id of block
    {
        int x = Mathf.FloorToInt(globalPos.x - position.x);
        int y = Mathf.FloorToInt(globalPos.y);
        int z = Mathf.FloorToInt(globalPos.z - position.z);

        return blockMap[x, y, z];
    }
    public void SetBlock(Vector3 globalPos, short blockID)  //put certain block and update mesh(for block placement and breaking)
    {
        int x = Mathf.FloorToInt(globalPos.x - position.x);
        int y = Mathf.FloorToInt(globalPos.y);
        int z = Mathf.FloorToInt(globalPos.z - position.z);

        blockMap[x, y, z] = blockID;

        UpdateChunk();
        UpdateSurrounding(new Vector3(x, y, z));
    }
    public void PutBlock(Vector3 globalPos, short blockID)  //put block without updating mesh
    {
        int x = Mathf.FloorToInt(globalPos.x - position.x);
        int y = Mathf.FloorToInt(globalPos.y);
        int z = Mathf.FloorToInt(globalPos.z - position.z);

        blockMap[x, y, z] = blockID;
    }

    public void UpdateSurrounding(Vector3 pos)  //update chunks adjacent to specified position
    {
        for(int p = 0; p < 6; p++)
        {
            Vector3 current = pos + Minecraft.adjacent[p];
            if(!IsInChunk(pos))
            {
                if(world.IsBlockInWorld(position + current))
                {
                    world.FindChunk(position+current).UpdateChunk();
                }
            }
        }
    }

    void AddBlockToData(Vector3 pos)
    {   //add block vertices, triangles and uvs to data of created mesh
        //pos is chunk-relative position of block
        for(int f = 0; f < 6; f++)  //for every face
        {
            short blockID = blockMap[(int)pos.x, (int)pos.y, (int)pos.z];

            if(!IsOpaque(pos + Minecraft.adjacent[f]))    //if adjacent block is transparent
            {
                for(int v = 0; v < 4; v++)  //fore every vertex
                {
                    //adding vertex to listW
                    vertices.Add(pos + //base position
                                //+ offset
                                Minecraft.vertices[ //select vertex
                                    Minecraft.triangles[f, v]   //depending on triangle
                                ]);
                }

                AddTexture(world.blockTypes[blockID].GetTextureIndex(f));

                //indices of vertices for both tiangles of face in clockwise order
                //utilizing the repetance of two vertices
                triangles.Add(vIndex);
                triangles.Add(vIndex + 1);
                triangles.Add(vIndex + 2);
                triangles.Add(vIndex + 2);
                triangles.Add(vIndex + 1);
                triangles.Add(vIndex + 3);

                vIndex += 4;
            }
        }
    }
    void AddTexture(int textureIndex)
    {   //add to uvs of mesh uvs of certain block texture
        //textureIndex is index of that block texture in the texture map
        //indices starts with 0 and go left-to-right top-to-bottom

        //get coordinates of requested texture piece
        if(textureIndex >= 0)
        {
            float x = (textureIndex % Minecraft.TextureSize) * Minecraft.NormalizedBlockTextureSize;
            float y = 1 - Minecraft.NormalizedBlockTextureSize * (textureIndex / Minecraft.TextureSize + 1);

            for(int i = 0; i < 4; i++)  //add uvs for texture piece
            {
                uvs.Add(new Vector2(x, y) + Minecraft.uvs[i] * Minecraft.NormalizedBlockTextureSize);
            }
        }
        
    }
    public void UpdateChunk()   //updating of mesh data
    {
        while(modifications.Count > 0)  //apply all queued modifications
        {
            BlockMod mod = modifications.Dequeue();

            PutBlock(mod.position, mod.id);
        }

        ClearMeshData();    //clear lists

        //add all blocks
        for(int x = 0; x < Minecraft.ChunkW; x++)
        {
            for(int z = 0; z < Minecraft.ChunkW; z++)
            {
                for(int y = 0; y < Minecraft.ChunkH; y++)
                {
                    Vector3 pos = new Vector3(x, y, z);
                    if(IsSolid(pos))    //now it's just solid blocks that added to mesh
                    {
                        AddBlockToData(pos);
                    }
                }
            }
        }

        CreateMesh();
    }
    void ClearMeshData()
    {
        vIndex = 0;
        vertices.Clear();
        triangles.Clear();
        uvs.Clear();
    }
    void CreateMesh()
    {
        //adding parsed values to mesh
        Mesh mesh = new Mesh();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.uv = uvs.ToArray();
        mesh.RecalculateNormals();

        mFilter.mesh = mesh;    //assigning mesh to filter
    }
}

public class Coordinates
{   //this class represents chunk coordinates in world space
    public int x;
    public int z;

    public Coordinates()
    {
        x = 0;
        z = 0;
    }
    public Coordinates(int _x, int _z)
    {
        x = _x; 
        z = _z;
    }
    public Coordinates(Vector3 pos)
    {
        x = Mathf.FloorToInt(pos.x) / Minecraft.ChunkW;
        z = Mathf.FloorToInt(pos.z) / Minecraft.ChunkW;
    }
    //next functions are necessary to use Coordinates as key in Dictionaty
    public override bool Equals(object obj)
    {
        if (obj == null || GetType() != obj.GetType())
            return false;
        
        return Equals(obj as Coordinates);
    }
    public bool Equals(Coordinates other)
    {
        if(other == null)
            return false;
        return x == other.x && z == other.z;
    }
    public override int GetHashCode()
    {   //Szudzik's Function
        //as far as I can find this is the best hash function for pair of ints
        //actually it can overflow int, but since coordinates probably won't be bigger then ~512 it is unlikely
        uint X = (uint)(x >= 0 ? 2 * x : -2 * x - 1);
        uint Z = (uint)(z >= 0 ? 2 * z : -2 * z - 1);
        int H = (int)((X >= Z ? X * X + X + Z : X + Z * Z) / 2);
        return x < 0 && z < 0 || x >= 0 && z >= 0 ? H : -H - 1;
    }
}