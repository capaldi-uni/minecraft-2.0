﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Biome", menuName = "Minecraft 2/Biome", order = 0)]
public class Biome : ScriptableObject   //this class store preset for biome
{
    new public string name;

    [Header("Terrain")]
    public int baseTerrainHeight;   //lowest point of terrain
    public int maxTerrainOffset;    //how far from lowest point it can go

    public float terrainScale;      //scale for noise

    public int terrainBlolck;   //topmost block
    public int subterrainBlock; //block under it
    public int subterrainHeight;    //width of subterrain block layer

    [Header("Trees")]   //values for perlin noise to determine how to place trees
    public float TreeZoneScale;
    public float TreeZoneOffset;
    [Range(0f, 1f)]
    public float TreeZoneThreshold;

    public float TreePlacementScale;
    public float TreePlacementOffset;
    [Range(0f, 1f)]
    public float TreePlacementThreshold;

    public int minTreeSize;
    public int maxTreeSize;


    [Header("Features")]
    public OreType[] ores;  //possible ore lodes

}
