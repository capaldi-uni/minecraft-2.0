﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Minecraft	//to store lookup tables and constants for chunk mesh creation
{
	public static readonly int ChunkW = 16;
	public static readonly int ChunkH = 128;
	public static readonly int WorldSizeC = 256;	//in chunks
	public static int WorldSizeB				//in blocks
	{
		get { return WorldSizeC * ChunkW; }
	}
	public static readonly int TextureSize = 4;	//in blocks
	public static float NormalizedBlockTextureSize	//size of block face texture (in units)
	{
		get {return 1f/ (float)TextureSize;}	//calculating it this way since BaseData is static basicaly
	}

	public static readonly int ViewDistanceC = 6;

	public static readonly Vector3[] vertices = new Vector3[8]
	{	//block vertices offsets
		new Vector3(0.0f, 0.0f, 0.0f),
		new Vector3(0.0f, 0.0f, 1.0f),
		new Vector3(0.0f, 1.0f, 0.0f),
		new Vector3(0.0f, 1.0f, 1.0f),
		new Vector3(1.0f, 0.0f, 0.0f),
		new Vector3(1.0f, 0.0f, 1.0f),
		new Vector3(1.0f, 1.0f, 0.0f),
		new Vector3(1.0f, 1.0f, 1.0f)
	};
	
	public static readonly int[,] triangles = new int[6, 4]
	{	//block triangles vertices by faces
		//each face has two triangles, so 6 vertices total, but two of them repeats and can be reused
		{0, 2, 4, 6},	//back face
		{5, 7, 1, 3},	//front face
		{4, 5, 0, 1},	//bottom face
		{2, 3, 6, 7},	//top face
		{1, 3, 0, 2},	//left face
		{4, 6, 5, 7}	//right face
	};

	public static readonly Vector2[] uvs = new Vector2[4]
	{	//texture uvs offsets for one face, same order as for triangles
		new Vector2(0.0f, 0.0f),
		new Vector2(0.0f, 1.0f),
		new Vector2(1.0f, 0.0f),
		new Vector2(1.0f, 1.0f)
	};

	public static readonly Vector3[] adjacent = new Vector3[6]
	{	//offsets of blocks adjacent to each face (faces order same as in triangles)
		new Vector3(0.0f, 0.0f, -1.0f),	//back face
		new Vector3(0.0f, 0.0f,  1.0f),	//front face
		new Vector3(0.0f, -1.0f, 0.0f),	//bottom face
		new Vector3(0.0f,  1.0f, 0.0f),	//top face
		new Vector3(-1.0f, 0.0f, 0.0f),	//left face
		new Vector3( 1.0f, 0.0f, 0.0f)	//right face
	};
}
