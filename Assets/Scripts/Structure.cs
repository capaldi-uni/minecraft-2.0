﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Structure   //this class handles generation of structures(just tree now)
{
    public static void Tree(Vector3 pos, Queue<BlockMod> q, int minHeight, int maxHeight)
    {
        //get "random" height
        int height = (int)(maxHeight * Noise.Get2D(new Vector2(pos.x, pos.z), 999, 3));

        if(height < minHeight)
            height = minHeight;

        // trunk //
        for(int i = 1; i < height; i++) 
            q.Enqueue(new BlockMod(new Vector3(pos.x, pos.y + i, pos.z), 8));
        
        
        // foliage //
        //bottom cuboid
        for (int y = -height/2; y < -1; y++)
        {
            for(int x = -2; x <= 2; x++)
            {
                for(int z = -2; z <= 2; z++)
                {
                    if(x != 0 || z != 0)
                        q.Enqueue(new BlockMod(new Vector3(pos.x + x, pos.y + y + height, pos.z + z), 9));
                }
            }
        }

        for(int x = -1; x <= 1; x++)
        {
            for(int z = -1; z <= 1; z++)
            {
                //under the top rectangle
                if(x != 0 || z != 0)
                    q.Enqueue(new BlockMod(new Vector3(pos.x + x, pos.y - 1 + height, pos.z + z), 9));

                //top plus
                if(x == 0 || z == 0)
                    q.Enqueue(new BlockMod(new Vector3(pos.x + x, pos.y + height, pos.z + z), 9));
            }
        }

    }
}
